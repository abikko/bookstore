package com.instagram.lavilla_sweets.android.ui.make_order;

public class Order {
    private String bookName, address, time, date,clientPhoneNumber;
    private int quantity;

    private static int gen_id;
    private int id;

    public Order() {
        gen_id++;
    }

    public Order(String confectionaryName, String address, String time, String date, String clientPhoneNumber, int quantity) {
        this();
        this.id = gen_id;
        this.bookName = confectionaryName;
        this.address = address;
        this.time = time;
        this.date = date;
        this.clientPhoneNumber = clientPhoneNumber;
        this.quantity = quantity;
    }

    public static int getGen_id() {
        return gen_id;
    }

    public static void setGen_id(int gen_id) {
        Order.gen_id = gen_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClientPhoneNumber() {
        return clientPhoneNumber;
    }

    public void setClientPhoneNumber(String clientPhoneNumber) {
        this.clientPhoneNumber = clientPhoneNumber;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
