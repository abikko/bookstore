package com.instagram.lavilla_sweets.android.ui.make_order;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.instagram.lavilla_sweets.android.R;

public class MakeOrderFragment extends Fragment {

    private EditText editTextAddress,editTextTime,editTextDate,editTextQuantity, editTextBookName;

    private DatabaseReference databaseReference;
    private FirebaseDatabase database;
    private FirebaseAuth mAuth;
    public MakeOrderFragment() {
        // Required empty public constructor
    }

    public static MakeOrderFragment newInstance(String param1, String param2) {
        MakeOrderFragment fragment = new MakeOrderFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_make_order, container, false);
        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference();



        editTextBookName = (EditText) v.findViewById(R.id.edit_text_make_order_confectionary_name);
        editTextDate = (EditText) v.findViewById(R.id.edit_text_make_order_date);
        editTextQuantity = (EditText) v.findViewById(R.id.edit_text_make_order_quantity);
        editTextAddress = (EditText) v.findViewById(R.id.edit_text_make_order_text);
        editTextTime = (EditText) v.findViewById(R.id.edit_text_make_order_time);



        Button buttonMakeOrder = (Button) v.findViewById(R.id.button_make_order);
        buttonMakeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Order newOrder = new Order(
                        editTextBookName.getText().toString(),
                        editTextAddress.getText().toString(),
                        editTextTime.getText().toString(),
                        editTextDate.getText().toString(),
                        mAuth.getCurrentUser().getPhoneNumber().toString(),
                        Integer.parseInt(editTextQuantity.getText().toString())
                );

                try {
                    databaseReference.child("order").push().setValue(newOrder);
                    Toast.makeText(getContext(),"Order was added", Toast.LENGTH_LONG).show();
                }catch (Exception e){
                    Toast.makeText(getContext(),"You have a trouble" + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
        return v;
    }
}