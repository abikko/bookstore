package com.instagram.lavilla_sweets.android.ui.settings;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.instagram.lavilla_sweets.android.R;

public class SettingsFragment extends Fragment implements View.OnClickListener{

   private FirebaseAuth mAuth;

    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance(String param1, String param2) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_settings, container, false);
        LinearLayout signOut = (LinearLayout) v.findViewById(R.id.linear_layout_settings_sign_out);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.linear_layout_settings_sign_out:{
                mAuth.signOut();
                break;
            }
            case R.id.linear_layout_settings_delete_account:{
                mAuth.getCurrentUser().delete();
                break;
            }
        }
    }
}