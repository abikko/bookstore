package com.instagram.lavilla_sweets.android.ui.orders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.instagram.lavilla_sweets.android.R;
import com.instagram.lavilla_sweets.android.ui.make_order.Order;
import com.instagram.lavilla_sweets.android.ui.orders.RecyclerView.OrderAdapter;

import java.util.ArrayList;

public class OrderFragment extends Fragment {

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private FirebaseAuth mAuth;
    private ArrayList<Order> orders;

    private Button buttonSort, buttonSearch;
    private EditText getQuantity;

    private RecyclerView recyclerView;
    private OrderAdapter orderAdapter;

    public OrderFragment() {
        // Required empty public constructor
    }

    public static OrderFragment newInstance() {
        OrderFragment fragment = new OrderFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_order, container, false);
        mAuth = FirebaseAuth.getInstance();

        orders = new ArrayList<>();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("order");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    if (mAuth.getCurrentUser().getPhoneNumber().toString().equals("+77479094214")) {
                        orders.add(dataSnapshot1.getValue(Order.class));
                        Log.d("Order", "onDataChange: new data added to admin panel");
                    } else {
                        if (dataSnapshot1.getValue(Order.class).getClientPhoneNumber() == mAuth.getCurrentUser().getPhoneNumber()) {
                            orders.add(dataSnapshot1.getValue(Order.class));
                            Log.d("Order", "onDataChange: new data added to a user panel");
                        }
                    }

                    orderAdapter = new OrderAdapter(orders);

                    recyclerView.setAdapter(orderAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("Error", "onCancelled: Failed to read value " + databaseError.toException());
            }
        });

        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view_order);
        orderAdapter = new OrderAdapter(orders);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(orderAdapter);
        return v;
    }
}