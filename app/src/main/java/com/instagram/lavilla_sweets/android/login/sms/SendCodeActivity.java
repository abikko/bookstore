package com.instagram.lavilla_sweets.android.login.sms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthProvider;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.instagram.lavilla_sweets.android.MainActivity;
import com.instagram.lavilla_sweets.android.R;
import com.instagram.lavilla_sweets.android.login.verify.VerifyActivity;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import java.util.concurrent.TimeUnit;

public class SendCodeActivity extends AppCompatActivity {

    private Button sendCodeButton;
    private EditText editTextPhoneNumber;
    private CountryCodePicker ccp;

    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_code);
        mAuth = FirebaseAuth.getInstance();
        if(mAuth.getCurrentUser() != null){
            startActivity(new Intent(this,MainActivity.class));
        }


        sendCodeButton = findViewById(R.id.button_send_code);
        ccp = findViewById(R.id.spinner_send_code_country_code);
        editTextPhoneNumber = findViewById(R.id.edit_text_send_code_phone_number);

        sendCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = ccp.getSelectedCountryCodeWithPlus() + editTextPhoneNumber.getText().toString().trim();

                Intent intent = new Intent(SendCodeActivity.this, VerifyActivity.class);
                intent.putExtra("phoneNumber",phoneNumber);
                startActivity(intent);
                finish();
            }
        });
    }

}