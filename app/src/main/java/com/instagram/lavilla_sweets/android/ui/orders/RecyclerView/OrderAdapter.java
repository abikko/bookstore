package com.instagram.lavilla_sweets.android.ui.orders.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.instagram.lavilla_sweets.android.R;
import com.instagram.lavilla_sweets.android.ui.make_order.Order;

import java.util.ArrayList;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderViewHolder> {

    private ArrayList<Order> orderList;

    public OrderAdapter(ArrayList<Order> orderList) {
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_row,parent,false);

        return new OrderViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int position) {
        Order currentOrder = orderList.get(position);

        holder.quantity.setText(currentOrder.getQuantity()+"");
        holder.date.setText(currentOrder.getDate()+"");
        holder.time.setText(currentOrder.getTime()+"");
        holder.text.setText(currentOrder.getAddress()+"");
        holder.confectionaryName.setText(currentOrder.getConfectionaryName()+"");
        holder.phoneNumber.setText(currentOrder.getClientPhoneNumber()+"");
        holder.id.setText(currentOrder.getId()+"");
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }
    protected static class OrderViewHolder extends RecyclerView.ViewHolder{

        private TextView confectionaryName, text, time, date, phoneNumber, quantity, id;

        public OrderViewHolder(@NonNull View itemView) {
            super(itemView);

            id = itemView.findViewById(R.id.text_view_order_row_id);
            confectionaryName = itemView.findViewById(R.id.text_view_order_row_confectionary_name);
            text = itemView.findViewById(R.id.text_view_order_row_text_big);
            time = itemView.findViewById(R.id.text_view_order_row_time_big);
            date = itemView.findViewById(R.id.text_view_order_row_date_big);
            phoneNumber = itemView.findViewById(R.id.text_view_order_row_phone_number_big);
            quantity = itemView.findViewById(R.id.text_view_order_row_quantity_big);
        }
    }
}

