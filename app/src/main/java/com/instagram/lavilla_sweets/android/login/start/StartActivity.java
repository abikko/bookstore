package com.instagram.lavilla_sweets.android.login.start;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.instagram.lavilla_sweets.android.R;
import com.instagram.lavilla_sweets.android.login.sms.SendCodeActivity;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(StartActivity.this, SendCodeActivity.class));
            }
        },1000);
    }
}